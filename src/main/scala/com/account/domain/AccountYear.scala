package com.account.domain

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import scala.beans.BeanProperty

import org.hibernate.validator.constraints.NotEmpty

import java.util.Date
import org.hibernate.validator.constraints.Length

@Entity
class AccountYear {
  
  @Id
  @GeneratedValue
  @BeanProperty
  var id: Long = _
  
  @NotEmpty
  @Length(max=4)
  @BeanProperty
  var year: String = _

  @Temporal(TemporalType.TIMESTAMP)
  @BeanProperty
  var regDate: Date = _
  
}