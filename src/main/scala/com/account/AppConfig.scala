package com.account

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.SpringApplication

/**
  * Created by enosent on 2016. 4. 18..
  */

object AppConfig extends App {
  SpringApplication.run(classOf[AppConfig])
}

@SpringBootApplication
class AppConfig