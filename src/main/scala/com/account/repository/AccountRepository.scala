package com.account.repository

import org.springframework.data.repository.CrudRepository
import com.account.domain.AccountYear
import java.lang.Long

trait AccountRepository extends CrudRepository[AccountYear, Long]