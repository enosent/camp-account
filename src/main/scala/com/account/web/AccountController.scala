package com.account.web

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.beans.factory.annotation.Autowired
import com.account.repository.AccountRepository
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMethod
import com.account.domain.AccountYear
import javax.validation.Valid
import org.springframework.validation.BindingResult
import java.util.Date

@Controller
@RequestMapping(Array("/accounts"))
class AccountController @Autowired()(private val accountRepository: AccountRepository) {
  
  @RequestMapping(method = Array(RequestMethod.GET))
  def list(model: Model) = {
    model.addAttribute("accounts", accountRepository.findAll())
    "account/list"
  }
  
  @RequestMapping(value=Array("/form"), method = Array(RequestMethod.GET))
  def form(model: Model) = {
    model.addAttribute("accountYear", new AccountYear)
    "account/form"
  }
  
  @RequestMapping(value=Array("/form"), method = Array(RequestMethod.POST))
  def create(@Valid accountYear: AccountYear, bindingResult: BindingResult) = {
    if (bindingResult.hasErrors()) {
      "account/form"
    } else {
      accountYear.regDate = new Date
      
      accountRepository.save(accountYear)
      "redirect:/accounts"
    }
  }
  
  // edit GET
  
  // edit POST
  
  // delete 
  
  // view GET
  
}